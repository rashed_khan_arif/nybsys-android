package com.nybsys.locationsharing

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

class LocationUpdateService : IntentService("Location Update"),
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    var locationRequest: LocationRequest? = null
    private val LOCATION_UPDATE_INTERVAL: Long = 1000 * 10
    var googleApiClient: GoogleApiClient? = null

    override fun onHandleIntent(intent: Intent?) {
        if (googleApiClient?.isConnected == true) {
            startLocationUpdate()
        }
    }


    private fun createLocationRequest() {
        locationRequest = LocationRequest().apply {
            interval = LOCATION_UPDATE_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createLocationRequest()
        configureGoogleApiClient()
        googleApiClient?.connect()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun configureGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(applicationContext)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
    }

    override fun onConnected(bundle: Bundle?) {
        Log.d("LOCATION_NYB", "Google Api Client Connected")
        startLocationUpdate()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d("LOCATION_NYB", "Google Api Client Failed to Connect")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d("LOCATION_NYB", "Google Api Client Suspended")
    }

    private fun startLocationUpdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest) {
            Log.d("LOCATION_NYB", "${it.latitude},${it.longitude}")
            val intent = Intent()
            intent.action = packageName + "location"
            intent.putExtra("lat", it.latitude)
            intent.putExtra("lng", it.longitude)
            sendBroadcast(intent)
        }
    }
}