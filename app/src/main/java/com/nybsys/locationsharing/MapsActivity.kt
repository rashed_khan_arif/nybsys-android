package com.nybsys.locationsharing

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    val locationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val lat = intent?.getStringExtra("lat")?.toDouble()
            val lng = intent?.getStringExtra("lng")?.toDouble()
            lng?.let { ln ->
                lat?.let { lt ->
                    updateLocationOnMap(lt, ln)
                    publishLocation(lt, ln)
                }
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        startService(Intent(this, LocationUpdateService::class.java))
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter()
        intentFilter.addAction(packageName + "location")
        registerReceiver(locationReceiver, intentFilter)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }


    private fun updateLocationOnMap(lat: Double, lng: Double) {
        mMap.addMarker(MarkerOptions().position(LatLng(lat, lng)))
        mMap.animateCamera(CameraUpdateFactory.newLatLng(LatLng(lat, lng)))
    }


    override fun onStop() {
        super.onStop()
        unregisterReceiver(locationReceiver)
    }

    private fun publishLocation(lat: Double, lng: Double) {
        val fireStore = FirebaseFirestore.getInstance()
        val locations: CollectionReference = fireStore.collection("locations")
        val userLocation = UserLocation(1, lat, lng)

        locations.add(userLocation).addOnCompleteListener(
            this
        ) {

            //
        }

        locations.addSnapshotListener { querySnapshot, firebaseFirestoreException ->
            if (firebaseFirestoreException != null) {
                return@addSnapshotListener
            }

            for (doc in querySnapshot!!.documentChanges) {
                when (doc.type) {
                    DocumentChange.Type.ADDED -> {
                        val location = doc.document.data
                        val userId = location["userId"] as Int

                        if (userId > 1) {
                            updateLocationOnMap(
                                location["lat"] as Double,
                                location["lng"] as Double
                            )
                        }
                    }
                    else -> {

                    }
                }
            }
        }

    }

}
