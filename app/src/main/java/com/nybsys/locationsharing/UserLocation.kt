package com.nybsys.locationsharing

data class UserLocation(
    var userId: Int,
    var lat: Double,
    var lng: Double
) {
}